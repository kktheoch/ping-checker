/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ping.check.app.exception.InputValidationException;
import ping.check.validator.IPAddressValidator;

/**
 *
 * @author kkthe
 */
public class IPValidatorTest {

    @Test
    public void testValidIPs() throws InputValidationException {
        IPAddressValidator.validateIP("127.126.126.2", false);
        IPAddressValidator.validateIP("255.255.255.255", false);
        IPAddressValidator.validateIP("0.0.0.0", false);
        IPAddressValidator.validateIP("     220.25.62.16", false);
        IPAddressValidator.validateIP("154.58.1.254 ", false);
        IPAddressValidator.validateIP("0.0.1.0", false);
    }

    @Test
    public void testInvalidIPs() {
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP(null, false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("126.212.125.", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP(".151.26.21", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("20.61.21a.60", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("161.216.123.210.", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("15.161.216.3051", false));
        Assertions.assertThrows(InputValidationException.class, ()
                -> IPAddressValidator.validateIP("0.0.0.256", false));
    }

}
