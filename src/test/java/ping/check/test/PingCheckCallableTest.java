/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ping.check.app.PingCheckCallable;
import ping.check.model.PingAddress;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class PingCheckCallableTest {

    @Test
    public void testNullAddress() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new PingCheckCallable(null));
    }

    @Test
    public void testLocalhostReachable() throws Exception {
        PingAddress localhost = PingAddress.from("test", "127.0.0.1", true);
        PingCheckCallable pingCallable = new PingCheckCallable(localhost);
        PingResponse call = pingCallable.call();
        Assertions.assertTrue(call.isSuccessful());
    }

    @Test
    public void testInvalidHostUnreachable() throws Exception {
        PingAddress localhost = PingAddress.from("test", "256.0.0.0", true);
        PingCheckCallable pingCallable = new PingCheckCallable(localhost);
        PingResponse call = pingCallable.call();
        Assertions.assertFalse(call.isSuccessful());
    }

}
