/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ping.check.model.PingAddress;
import ping.check.model.PingGUICounters;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class UpdatePingRunnableTest {

    private PingGUICounters pingGUI;
    private static final DecimalFormat AVG_FORMATTER
            = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);

    @BeforeEach
    public void init() {
        AVG_FORMATTER.applyPattern("#.##");
        this.pingGUI = new PingGUICounters(new LinkedList<>());
    }

    @Test
    public void testNullList() {
        this.pingGUI = new PingGUICounters(null);
        Assertions.assertNotNull(this.pingGUI);
        Assertions.assertEquals("0 ms", this.pingGUI.getAverageLabel().getText());
        Assertions.assertEquals("0 ms", this.pingGUI.getMaxStr().getText());
        Assertions.assertEquals("0 ms", this.pingGUI.getMinStr().getText());
    }

    @Test
    public void testAveragePing() {
        List<PingResponse> list = new ArrayList<>();
        double total = 0D;
        for (int i = 0; i < 100; i++) {
            long nextLong = ThreadLocalRandom.current().nextLong(1000L);
            list.add(getPingResponse(true, nextLong));
            total += nextLong;
        }
        this.pingGUI = new PingGUICounters(list);
        Assertions.assertEquals(AVG_FORMATTER.format(total / 100D) + " ms", this.pingGUI.getAverageLabel().getText());
        Assertions.assertEquals("100%", this.pingGUI.getReachablePercentage().getText());
    }

    @Test
    public void testMaxPing() {
        List<PingResponse> list = new ArrayList<>();
        long max = Long.MIN_VALUE;
        for (int i = 0; i < 100; i++) {
            long nextLong = ThreadLocalRandom.current().nextLong(1000L);
            list.add(getPingResponse(true, nextLong));
            if (max < nextLong) {
                max = nextLong;
            }
        }
        this.pingGUI = new PingGUICounters(list);
        Assertions.assertEquals(String.valueOf(max) + " ms", this.pingGUI.getMaxStr().getText());
        Assertions.assertEquals("100%", this.pingGUI.getReachablePercentage().getText());
    }

    @Test
    public void testMinPing() {
        List<PingResponse> list = new ArrayList<>();
        long min = Long.MAX_VALUE;
        for (int i = 0; i < 100; i++) {
            long nextLong = ThreadLocalRandom.current().nextLong(1000L);
            list.add(getPingResponse(true, nextLong));
            if (min > nextLong) {
                min = nextLong;
            }
        }
        this.pingGUI = new PingGUICounters(list);
        Assertions.assertEquals(String.valueOf(min) + " ms", this.pingGUI.getMinStr().getText());
        Assertions.assertEquals("100%", this.pingGUI.getReachablePercentage().getText());
    }

    @Test
    public void testReachableRound() {
        List<PingResponse> list = new ArrayList<>();
        list.add(getPingResponse(true, 0L));
        list.add(getPingResponse(true, 0L));
        list.add(getPingResponse(false, 0L));
        this.pingGUI = new PingGUICounters(list);
        Assertions.assertEquals("66.67%", this.pingGUI.getReachablePercentage().getText());
    }

    private PingResponse getPingResponse(boolean reachable, Long millis) {
        return PingResponse.getPingResponse(
                PingAddress.from("test", "test", true),
                reachable,
                millis
        );
    }

}
