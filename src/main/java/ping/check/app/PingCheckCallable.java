/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Objects;
import java.util.concurrent.Callable;
import ping.check.log.LogMngr;
import ping.check.model.PingAddress;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class PingCheckCallable implements Callable<PingResponse> {

    private static final int TIMEOUT = 3000;
    private final PingAddress address;

    public PingCheckCallable(PingAddress address) throws NullPointerException {
        this.address = Objects.requireNonNull(address, "Address can't be null");
    }

    @Override
    public PingResponse call() throws Exception {
        try {
            InetAddress inet = InetAddress.getByName(this.address.getHost());
            long start = System.currentTimeMillis();
            boolean reachable = inet.isReachable(TIMEOUT);
            return PingResponse.getPingResponse(this.address, reachable, (System.currentTimeMillis() - start));
        } catch (IOException ex) {
            LogMngr.error("Failed to connect to host " + this.address + " with exception", ex);
            return PingResponse.getPingResponse(this.address, false, 0L);
        }
    }

}
