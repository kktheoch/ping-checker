/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

/**
 *
 * @author kkthe
 */
public enum AppProperties {
    INSTANCE;

    public final String SECONDS_PER_PING_PROPERTY_KEY = "ping.seconds";
    public int SECONDS_PER_PING = Constants.DEFAULT_PING_EVERY_SECONDS;

    public final String SECONDS_PER_REFRESH_PROPERTY_KEY = "aggregate.seconds";
    public int SECONDS_PER_REFRESH = Constants.DEFAULT_MAX_PINGS;

}
