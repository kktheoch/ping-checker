/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import ping.check.gui.GUI;
import ping.check.model.PingAddress;
import ping.check.model.PingAggregationResponse;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class PingResponseConsumer implements Consumer<Future<PingResponse>> {

    final PingAggregationResponse aggregation;
    final Map<PingAddress, LinkedHashMap<Integer, PingResponse>> AVERAGE_QUEUE;

    public PingResponseConsumer() {
        this.aggregation = new PingAggregationResponse();
        this.AVERAGE_QUEUE = new HashMap<>();
    }

    public void setMaxPings(int maxPings) {
        aggregation.setMaxPing(maxPings);
    }

    @Override
    public void accept(Future<PingResponse> resp) {
        try {
            PingResponse pingResponse = resp.get();
            aggregation.addPing(pingResponse);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void printSum() {
        GUI.addPingInGUI(aggregation);
    }

}
