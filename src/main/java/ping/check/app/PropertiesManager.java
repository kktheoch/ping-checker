/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Properties;
import ping.check.app.exception.PropertiesException;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public enum PropertiesManager {
    INSTANCE;

    private static final String CONFIG_NAME = "config.properties";

    public void loadProperties() {
        File file = new File(CONFIG_NAME);
        if (!file.exists()) {
            LogMngr.error("Failed to load configuration file, creating default configuration file.");
            createDefaultConfigFile();
        }
        Properties properties;
        try {
            properties = loadPropertiesFromFile(file);
        } catch (PropertiesException ex) {
            LogMngr.error("Failed to load properties, falling back to defaults.", ex);
            return;
        }
    }
    
    public void setProperties(Properties loadedProperties) {
        
    }

    public Properties loadPropertiesFromFile(File file) throws PropertiesException {
        try (InputStream inputStream = new FileInputStream(file)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } catch (Exception ex) {
            throw new PropertiesException(ex);
        }
    }

    public void createDefaultConfigFile() {
        Properties properties = new Properties();
        properties.put("ping.seconds", String.valueOf(Constants.DEFAULT_PING_EVERY_SECONDS));
        properties.put("aggregate.seconds", String.valueOf(Constants.DEFAULT_MAX_PINGS));
        File props = new File(CONFIG_NAME);
        try {
            OutputStream out = new FileOutputStream(props);
            properties.store(out, "Default generated properties");
        } catch (Exception ex) {
            LogMngr.error("Failed to create config file", ex);
        }
    }
}
