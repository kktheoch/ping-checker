/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

import java.util.LinkedList;
import java.util.List;
import ping.check.model.PingAddress;

/**
 *
 * @author kkthe
 */
public final class Constants {

    public static final List<PingAddress> PING_IPS;
    public static int DEFAULT_PING_EVERY_SECONDS = 1;
    public static int DEFAULT_MAX_PINGS = 10;
    
    static {
        PING_IPS = new LinkedList<>();
        PING_IPS.add(PingAddress.from("Google", "8.8.8.8", true));
        PING_IPS.add(PingAddress.from("LoL EUNE 1", "104.160.142.2", true));
        PING_IPS.add(PingAddress.from("LoL EUNE 2", "104.160.142.3", true));
    }

}
