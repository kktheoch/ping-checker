/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import ping.check.log.LogMngr;
import ping.check.model.PingAddress;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class PingCheck {

    static long lastDelayPerPing;
    static long lastDelayPerConsume;
    static ScheduledExecutorService PING_COMMAND_EXECUTOR;
    static ExecutorService PING_EXECUTOR;
    static ScheduledExecutorService PING_CONSUMER_EXECUTOR;
    static PingResponseConsumer PING_CONSUMER = new PingResponseConsumer();
    static final ConcurrentLinkedQueue<Future<PingResponse>> PING_RESPONSES = new ConcurrentLinkedQueue<>();
    static volatile boolean isRunning;

    public static void startPing(long delayPerPing, long maxPingsToAggregate) throws InterruptedException {
        lastDelayPerPing = delayPerPing;
        lastDelayPerConsume = maxPingsToAggregate;
        // Default max pings is twice the acceptable pings in the timeframe
        // Example: Refresh of 30 seconds with 5 seconds ping intervals
        // (30/5)*2 = 12
        int maxPings = (int) maxPingsToAggregate;
        LogMngr.info("Max ping results to hold: " + maxPings);
        PING_CONSUMER.setMaxPings(maxPings);
        initPingCommands(delayPerPing);
        initConsumer();
        isRunning = true;
    }

    public static void stopPing() {
        stopPingCommands();
        stopConsumer();
        PING_RESPONSES.clear();
        isRunning = false;
    }

    public static synchronized boolean isRunning() {
        return isRunning;
    }

    private static void initPingCommands(long delayPerPing) {
        PING_COMMAND_EXECUTOR = Executors.newScheduledThreadPool(1);
        PING_EXECUTOR = Executors.newFixedThreadPool(1);
        PING_COMMAND_EXECUTOR.scheduleAtFixedRate(
                () -> {
                    Constants.PING_IPS.forEach((PingAddress pingAddress) -> {
                        if (pingAddress.isEnabled()) {
                            PING_RESPONSES.add(
                                    PING_EXECUTOR.submit(
                                            new PingCheckCallable(pingAddress)
                                    )
                            );
                        }
                    }
                    );
                },
                0,
                delayPerPing,
                TimeUnit.SECONDS);
    }

    private static void stopPingCommands() {
        PING_EXECUTOR.shutdown();
        PING_COMMAND_EXECUTOR.shutdown();
    }

    private static void initConsumer() throws InterruptedException {
        PING_CONSUMER_EXECUTOR = Executors.newScheduledThreadPool(1);
        PING_CONSUMER_EXECUTOR.scheduleAtFixedRate(
                () -> {
                    List<Future<PingResponse>> collected = PING_RESPONSES.stream().collect(Collectors.toList());
                    PING_RESPONSES.clear();
                    collected.forEach(PING_CONSUMER);
                    PING_CONSUMER.printSum();
                },
                0,
                1,
                TimeUnit.SECONDS);
    }

    private static void stopConsumer() {
        PING_CONSUMER_EXECUTOR.shutdown();
    }

}
