/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.app.exception;

import java.io.IOException;

/**
 *
 * @author kkthe
 */
public class PropertiesException extends IOException {

    public PropertiesException(Exception ex) {
        super(ex);
    }
}
