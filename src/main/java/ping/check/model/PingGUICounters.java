/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.model;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author kkthe
 */
public class PingGUICounters {

    private final List<PingResponse> pingResponses;
    private static final DecimalFormat AVG_FORMATTER = 
            (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);


    public PingGUICounters(List<PingResponse> responses) {
        AVG_FORMATTER.applyPattern("#.##");
        if (responses == null) {
            this.pingResponses = Collections.EMPTY_LIST;
            return;
        }
        this.pingResponses = responses;
    }

    private Double getAverage() {
        return pingResponses.stream()
                .filter(x -> x.isSuccessful())
                .map(x -> x.getElapsedMillis())
                .collect(Collectors.averagingLong(x -> x));
    }

    public Text getAverageLabel() {
        Double average = getAverage();
        var text = new Text();
        if (average > 200.0) {
            text.setFill(Color.RED);
        } else if (average > 100.0) {
            text.setFill(Color.YELLOW);
        } else {
            text.setFill(Color.GREEN);
        }
        text.setText(AVG_FORMATTER.format(average) + " ms");
        return text;
    }
    
    private long getPingCount() {
        return pingResponses.stream().count();
    }
    
    public Text getPingCountLabel() {
        long pingCount = getPingCount();
        var text = new Text(String.valueOf(pingCount));
        return text;
    }

    private Long getMax() {
        Optional<PingResponse> max = pingResponses.stream()
                .filter(x -> x.isSuccessful())
                .max(Comparator.comparingDouble(PingResponse::getElapsedMillis));
        if (max.isPresent()) {
            return max.get().getElapsedMillis();
        }
        return 0L;
    }

    public Text getMaxStr() {
        Long max = getMax();
        return new Text(String.valueOf(max) + " ms");
    }

    private Long getMin() {
        Optional<PingResponse> min = pingResponses.stream()
                .filter(x -> x.isSuccessful())
                .min(Comparator.comparingDouble(PingResponse::getElapsedMillis));
        if (min.isPresent()) {
            return min.get().getElapsedMillis();
        }
        return 0L;
    }

    public Text getMinStr() {
        Long min = getMin();
        return new Text(String.valueOf(min) + " ms");
    }

    private Double getReachable() {
        int total = pingResponses.size();
        long successful = pingResponses.stream()
                .filter(x -> x.isSuccessful())
                .count();
        return (double) successful / (double) total;
    }

    public Text getReachablePercentage() {
        Double reachable = getReachable();
        var text = new Text();
        if (reachable < 0.8) {
            text.setFill(Color.RED);
        } else if (reachable == 1.0) {
            text.setFill(Color.GREEN);
        } else {
            text.setFill(Color.YELLOW);
        }
        text.setText(AVG_FORMATTER.format(getReachable() * 100.0) + "%");
        return text;
    }

}
