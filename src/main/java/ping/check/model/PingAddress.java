/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.model;

import java.util.Objects;

/**
 *
 * @author kkthe
 */
public class PingAddress {

    private final String name;
    private final String host;
    private boolean enabled;

    private PingAddress(String name, String host, boolean enabled) {
        this.name = name;
        this.host = host;
        this.enabled = enabled;
    }

    public static PingAddress from(String name, String host, boolean enabled) {
        return new PingAddress(name, host, enabled);
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void alterEnabled() {
        this.enabled = !this.enabled;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.name);
        hash = 31 * hash + Objects.hashCode(this.host);
        hash = 31 * hash + (this.enabled ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PingAddress other = (PingAddress) obj;
        if (this.enabled != other.enabled) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.host, other.host)) {
            return false;
        }
        return true;
    }

}
