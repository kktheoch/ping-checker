/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.model;

/**
 *
 * @author kkthe
 */
public class PingResponse {

    private final PingAddress pingAddress;
    private final boolean successful;
    private final Long elapsedMillis;

    private PingResponse(PingAddress pingAddress, boolean successful, Long elapsedMillis) {
        this.pingAddress = pingAddress;
        this.successful = successful;
        if (successful) {
            this.elapsedMillis = elapsedMillis;
        } else {
            this.elapsedMillis = null;
        }
    }

    public static PingResponse getPingResponse(PingAddress pingAddress, boolean successful, Long elapsedMillis) {
        return new PingResponse(pingAddress, successful, elapsedMillis);
    }

    public boolean isSuccessful() {
        return successful;
    }

    public Long getElapsedMillis() {
        return elapsedMillis;
    }

    public PingAddress getPingAddress() {
        return pingAddress;
    }

}
