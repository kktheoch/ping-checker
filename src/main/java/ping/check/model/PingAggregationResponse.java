/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kkthe
 */
public class PingAggregationResponse {

    private final Map<PingAddress, List<PingResponse>> pingMap;
    private static final int DEFAULT_MAX_PINGS = 5;
    private int maxPingResults;

    public PingAggregationResponse() {
        this.pingMap = new HashMap<>();
        this.maxPingResults = DEFAULT_MAX_PINGS;
    }
    
    public void setMaxPing(int maxPingResults) {
        this.maxPingResults = maxPingResults;
    }

    public void addPing(PingResponse response) {
        // Add a new entry to the map if it doesnt exist.
        if (!pingMap.containsKey(response.getPingAddress())) {
            pingMap.put(response.getPingAddress(), new LinkedList<>());
        }
        // In this case the very first element of the list must be removed.
        if (pingMap.get(response.getPingAddress()) != null) {
            if (pingMap.get(response.getPingAddress()).size() >= maxPingResults) {
                pingMap.get(response.getPingAddress()).remove(0);
            }
            pingMap.get(response.getPingAddress()).add(response);
        }
    }

    public Map<PingAddress, List<PingResponse>> getPingMap() {
        return this.pingMap;
    }

}
