/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ping.check.app.Constants;
import ping.check.app.PingCheck;
import ping.check.app.exception.InputValidationException;
import ping.check.gui.button.handlers.CloseEventHandler;
import ping.check.gui.button.handlers.SelectBoxHandler;
import ping.check.gui.button.handlers.StartButtonHandler;
import ping.check.gui.util.BorderPaneUtil;
import ping.check.gui.util.PingGridUtil;
import ping.check.log.LogMngr;
import ping.check.model.PingAggregationResponse;
import ping.check.validator.InputValidation;

/**
 *
 * @author kkthe
 */
public class GUI extends Application {

    private static Stage STAGE;
    private static Scene SCENE;
    private static final BorderPane BORDER_PANE = new BorderPane();
    private static Button START_BUTTON;
    private static ComboBox<Integer> PING_SEC_INPUT;
    private static ComboBox<Integer> MAX_PING_INPUT;
    private static final List<Control> BUTTONS = new ArrayList<>();

    public static void startGUI(String[] args) {
        launch(args);
    }

    public static final BorderPane getBorderPane() {
        return BORDER_PANE;
    }

    public static void addPingInGUI(PingAggregationResponse pingAggregation) {
        Platform.runLater(
                new UpdatePingRunnable(pingAggregation, BORDER_PANE)
        );
    }

    public static void hideGUI() {
        Platform.runLater(() -> {
            GUI.STAGE.hide();
        });
    }

    public static void showGUI() {
        Platform.runLater(() -> {
            GUI.STAGE.show();
        });
    }

    public static void stopApplication() {
        Platform.runLater(() -> {
            GUI.STAGE.close();
        });
        Platform.exit();
        System.exit(0);
    }

    @Override
    public void start(Stage stage) throws Exception {
        try {
            Platform.setImplicitExit(false);
            GUI.STAGE = stage;
            GUI.STAGE.getIcons().add(new Image("/app-icon.png"));
            initGUIPanel();
            addStartButton();
            addInputs();
            GUI.STAGE.setOnCloseRequest(new CloseEventHandler(null, (StartButtonHandler) START_BUTTON.getOnAction()));
            checkDisabledStart();
            display();
            LogMngr.info("Initiated GUI succesfully.");
        } catch (Exception ex) {
            LogMngr.error("Initiated GUI failed with exception ", ex);
            throw ex;
        }
    }

    @Override
    public void stop() throws Exception {
        if (PingCheck.isRunning()) {
            PingCheck.stopPing();
        }
        super.stop();
    }

    private void initGUIPanel() {
        BorderPaneUtil.INSTANCE.initBorderPane();
        SCENE = new Scene(BORDER_PANE, 500, 500);
        SCENE.getStylesheets().add("main-style.css");
        STAGE.setTitle("Ping Check");
        STAGE.setScene(SCENE);
        LogMngr.info("Initiated GUI Panel.");
    }

    private void addStartButton() {
        START_BUTTON = new Button();
        START_BUTTON.setMinSize(60, 40);
        START_BUTTON.setText("Start");
        START_BUTTON.setOnAction(new StartButtonHandler(START_BUTTON));
        START_BUTTON.setDisable(true);
        var hbox = new HBox();
        hbox.setPadding(new Insets(0, 0, 30, 400));
        hbox.getChildren().add(START_BUTTON);
        BORDER_PANE.setBottom(hbox);
        LogMngr.info("Appended start button.");
    }

    private void addInputs() {
        var vboxContainer = new VBox();
        vboxContainer.setPadding(new Insets(10));
        vboxContainer.getChildren().add(getPingSecondsInputHBox());
        vboxContainer.getChildren().add(getRefreshSecondsInputHBox());
        vboxContainer.getChildren().add(new Separator(Orientation.HORIZONTAL));
        vboxContainer.getChildren().add(PingGridUtil.INSTANCE.initGridPane());
        vboxContainer.getChildren().add(new Separator(Orientation.HORIZONTAL));
        var vboxOutputContainer = new VBox();
        vboxOutputContainer.setId("output");
        vboxContainer.getChildren().add(vboxOutputContainer);
        BORDER_PANE.setCenter(vboxContainer);
        LogMngr.info("Appended input buttons.");
    }

    private void display() {
        STAGE.setResizable(false);
        STAGE.show();
        LogMngr.info("GUI is now visible.");
    }

    private HBox getPingSecondsInputHBox() {
        var pingLbl1 = new Label("Ping every ");
        PING_SEC_INPUT = new ComboBox(FXCollections.observableArrayList(1, 2, 3, 4, 5));
        PING_SEC_INPUT.setValue(Constants.DEFAULT_PING_EVERY_SECONDS);
        PING_SEC_INPUT.setTooltip(new Tooltip("Pings will execute every X seconds."));
        PING_SEC_INPUT.setId("ping-second-input");
        BUTTONS.add(PING_SEC_INPUT);
        PING_SEC_INPUT.setMaxSize(70, 20);
        PING_SEC_INPUT.setOnAction(new SelectBoxHandler(PING_SEC_INPUT));
        var pingLbl2 = new Label(" seconds.");
        var pingSecondsHBox = new HBox();
        pingSecondsHBox.setSpacing(5);
        pingSecondsHBox.getChildren().add(pingLbl1);
        pingSecondsHBox.getChildren().add(PING_SEC_INPUT);
        pingSecondsHBox.getChildren().add(pingLbl2);
        return pingSecondsHBox;
    }

    private HBox getRefreshSecondsInputHBox() {
        var pingLbl1 = new Label("Max # of pings to aggregate: ");
        MAX_PING_INPUT = new ComboBox(FXCollections.observableArrayList(10, 30, 50, 100));
        MAX_PING_INPUT.setValue(Constants.DEFAULT_MAX_PINGS);
        MAX_PING_INPUT.setTooltip(new Tooltip("Ping stats will be counted every X seconds."));
        MAX_PING_INPUT.setId("ping-aggr-second-input");
        MAX_PING_INPUT.setMaxSize(70, 20);
        MAX_PING_INPUT.setOnAction(new SelectBoxHandler(MAX_PING_INPUT));
        BUTTONS.add(MAX_PING_INPUT);
        var pingSecondsHBox = new HBox();
        pingSecondsHBox.setSpacing(5);
        pingSecondsHBox.getChildren().add(pingLbl1);
        pingSecondsHBox.getChildren().add(MAX_PING_INPUT);
        return pingSecondsHBox;
    }

    public static boolean checkDisabledStart() {
        LogMngr.info("Initiated check for start button.");
        boolean disabledStatus = true;
        try {
            getPingSeconds();
            getMaxPings();
            disabledStatus = !PingGridUtil.INSTANCE.isAnyIpEnabled();
        } catch (InputValidationException ex) {
            disabledStatus = true;
        }
        LogMngr.info("Button disabled status: " + disabledStatus);
        START_BUTTON.setDisable(disabledStatus);
        return disabledStatus;
    }

    public static void setInputEnabled(boolean enabled) {
        BUTTONS.stream().forEach(x -> x.setDisable(!enabled));
        PingGridUtil.INSTANCE.setButtonStatus(enabled);
    }

    public static int getPingSeconds() throws InputValidationException {
        return InputValidation.validateAndGetInput(Constants.DEFAULT_PING_EVERY_SECONDS, 0, 15);
    }

    public static int getMaxPings() throws InputValidationException {
        return InputValidation.validateAndGetInput(Constants.DEFAULT_MAX_PINGS, 10, 100);
    }

}
