/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javafx.event.Event;
import javafx.scene.control.Control;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import ping.check.app.PingCheck;
import ping.check.gui.GUI;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public class CloseEventHandler extends AbstractButtonHandler<WindowEvent> {

    private final StartButtonHandler startHandler;

    public CloseEventHandler(Control button, StartButtonHandler startHandler) {
        super(button);
        this.startHandler = startHandler;
    }

    @Override
    public void handle(Event event) {
        if (!SystemTray.isSupported()) {
            LogMngr.info("System tray is not supported.");
            return;
        }
        try {
            GUI.hideGUI();
            final SystemTray tray = SystemTray.getSystemTray();
            BufferedImage image = ImageIO.read(getClass().getResource("/tray-icon.png"));
            PopupMenu popup = new PopupMenu();
            MenuItem restoreItem = new MenuItem("Restore");
            MenuItem exitItem = new MenuItem("Close");
            MenuItem startItem = new MenuItem("Start");
            startItem.setEnabled(!PingCheck.isRunning() && !GUI.checkDisabledStart());
            MenuItem stopItem = new MenuItem("Stop");
            stopItem.setEnabled(PingCheck.isRunning());
            popup.add(startItem);
            popup.add(stopItem);
            popup.addSeparator();
            popup.add(restoreItem);
            popup.addSeparator();
            popup.add(exitItem);
            TrayIcon trayIcon = new TrayIcon(image, "Ping Check", popup);
            trayIcon.setImageAutoSize(true);
            ActionListener restoreListener = (java.awt.event.ActionEvent arg0) -> {
                GUI.showGUI();
                tray.remove(trayIcon);
            };
            ActionListener exitListener = (java.awt.event.ActionEvent arg) -> {
                GUI.stopApplication();
            };
            ActionListener startStopListener = (java.awt.event.ActionEvent arg) -> {
                startHandler.handleStartStop();
                startItem.setEnabled(!startItem.isEnabled());
                stopItem.setEnabled(!stopItem.isEnabled());
            };
            trayIcon.addActionListener(restoreListener);
            exitItem.addActionListener(exitListener);
            restoreItem.addActionListener(restoreListener);
            startItem.addActionListener(startStopListener);
            stopItem.addActionListener(startStopListener);
            tray.add(trayIcon);
        } catch (Exception ex) {
            LogMngr.error("Exception while adding tray icon", ex);
        }
    }

}
