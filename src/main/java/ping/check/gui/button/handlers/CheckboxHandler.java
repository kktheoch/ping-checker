/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Control;
import ping.check.gui.GUI;
import ping.check.log.LogMngr;
import ping.check.model.PingAddress;

/**
 *
 * @author kkthe
 */
public class CheckboxHandler extends AbstractButtonHandler<ActionEvent> {

    private final PingAddress ping;

    public CheckboxHandler(Control button, PingAddress ping) throws NullPointerException {
        super(button);
        this.ping = Objects.requireNonNull(ping, "Ping can't be null");
    }

    @Override
    public void handle(Event event) {
        ping.alterEnabled();
        GUI.checkDisabledStart();
        LogMngr.info("Address " + ping.getName() + " enabled: " + ping.isEnabled());
    }

}
