/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import java.util.Objects;
import javafx.css.PseudoClass;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import ping.check.app.exception.InputValidationException;

/**
 *
 * @author kkthe
 * @param <ActionEvent>
 */
public abstract class AbstractButtonHandler<ActionEvent> implements EventHandler {

    protected Control button;

    public AbstractButtonHandler(Control button) {
        this.button = button;
    }

    @Override
    public abstract void handle(Event event);

    protected void setEnabledButton(boolean enabled) {
        button.setDisable(!enabled);
    }

    protected void setButtonText(String text) throws NullPointerException {
        if (button instanceof Labeled) {
            ((Labeled) button).setText(Objects.requireNonNull(text, "Text can't be null."));
        }
    }

    protected void markInputAsError(Control textField, boolean error, InputValidationException ex) {
        final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
        if (ex != null) {
            textField.getTooltip().setText(ex.getMessage());
        } else {
            textField.getTooltip().setText("");
        }
        textField.pseudoClassStateChanged(errorClass, error);
    }
}
