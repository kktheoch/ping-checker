/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import ping.check.app.Constants;
import ping.check.app.exception.InputValidationException;
import ping.check.gui.util.PingGridUtil;
import ping.check.model.PingAddress;
import ping.check.validator.IPAddressValidator;
import ping.check.validator.NameValidator;

/**
 *
 * @author kkthe
 */
public class ConfirmIPHandler extends AbstractButtonHandler<ActionEvent> {

    private final TextField nameTextField;
    private final TextField ipTextField;

    public ConfirmIPHandler(Control button, TextField nameTextField, TextField ipTextField) {
        super(button);
        this.nameTextField = nameTextField;
        this.ipTextField = ipTextField;
    }

    @Override
    public void handle(Event event) {
        String name = null;
        boolean hasError = false;
        try {
            name = NameValidator.validate(nameTextField.getText());
            super.markInputAsError(nameTextField, false, null);
        } catch (InputValidationException ex) {
            super.markInputAsError(nameTextField, true, ex);
            hasError = true;
        }
        String ip = null;
        try {
            ip = IPAddressValidator.validateIP(ipTextField.getText(), true);
            super.markInputAsError(ipTextField, false, null);
        } catch (InputValidationException ex) {
            super.markInputAsError(ipTextField, true, ex);
            hasError = true;
        }
        if (hasError) {
            return;
        }
        PingAddress newPingAddress = PingAddress.from(name, ip, true);
        Constants.PING_IPS.add(newPingAddress);
        super.setEnabledButton(false);
        PingGridUtil.INSTANCE.redraw();
    }

}
