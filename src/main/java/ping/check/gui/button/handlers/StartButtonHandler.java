/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Button;
import ping.check.app.PingCheck;
import ping.check.gui.GUI;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public class StartButtonHandler extends AbstractButtonHandler<ActionEvent> {

    public StartButtonHandler(Button button) {
        super(button);
    }

    @Override
    public void handle(Event event) {
        handleStartStop();
    }

    public void handleStartStop() {
        LogMngr.info("Received event action. Current status isRunning: " + PingCheck.isRunning());
        super.setEnabledButton(false);
        if (PingCheck.isRunning()) {
            LogMngr.info("Stopping ping mechanism.");
            stopPing();
            GUI.setInputEnabled(true);
        } else {
            LogMngr.info("Starting ping mechanism.");
            startPing();
            GUI.setInputEnabled(false);
        }
        LogMngr.info("Event handled succesfully. Current status isRunning: " + PingCheck.isRunning());
        super.setEnabledButton(true);
    }

    private void startPing() {
        try {
            PingCheck.startPing(GUI.getPingSeconds(), GUI.getMaxPings());
            super.setButtonText("Stop");
        } catch (Exception ex) {
            LogMngr.error("Starting ping failed with exception", ex);
        }
    }

    private void stopPing() {
        try {
            System.out.println("Stop event initiated.");
            PingCheck.stopPing();
            super.setButtonText("Start");
        } catch (Exception ex) {
            LogMngr.error("Stop ping failed with exception", ex);
        }
    }

}
