/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.ComboBox;
import ping.check.app.Constants;
import ping.check.app.exception.InputValidationException;
import ping.check.gui.GUI;
import ping.check.log.LogMngr;
import ping.check.validator.InputValidation;

/**
 *
 * @author kkthe
 */
public class SelectBoxHandler extends AbstractButtonHandler<ActionEvent> {

    public SelectBoxHandler(ComboBox<Integer> control) {
        super(control);
    }

    @Override
    public void handle(Event event) {
        Integer selected = ((ComboBox<Integer>) button).getValue();
        if ("ping-second-input".equals(button.getId())) {
            handlePingSecondsInput(selected);
        } else if ("ping-aggr-second-input".equals(button.getId())) {
            handleMaxPings(selected);
        }
        GUI.checkDisabledStart();
    }

    private void handlePingSecondsInput(Integer selectedValue) {
        try {
            Constants.DEFAULT_PING_EVERY_SECONDS
                    = InputValidation.validateAndGetInput(selectedValue, 0, 15);
            super.markInputAsError(button, false, null);
        } catch (InputValidationException ex) {
            LogMngr.error("Input validation exception", ex);
            super.markInputAsError(button, true, ex);
        }
    }

    private void handleMaxPings(Integer selectedValue) {
        try {
            Constants.DEFAULT_MAX_PINGS
                    = InputValidation.validateAndGetInput(selectedValue, 10, 100);
            super.markInputAsError(button, false, null);
        } catch (InputValidationException ex) {
            LogMngr.error("Input validation exception", ex);
            super.markInputAsError(button, true, ex);
        }
    }

}
