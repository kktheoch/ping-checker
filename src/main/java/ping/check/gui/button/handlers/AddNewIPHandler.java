/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.button.handlers;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import ping.check.validator.IPAddressValidator;

/**
 *
 * @author kkthe
 */
public class AddNewIPHandler extends AbstractButtonHandler<ActionEvent> {

    private final GridPane gridPane;
    private final int rowToBeAdded;

    public AddNewIPHandler(Control button, GridPane grid) {
        super(button);
        this.gridPane = grid;
        this.rowToBeAdded = gridPane.getRowCount();
    }

    @Override
    public void handle(Event event) {
        removeOldRow();
        appendNewIPRow();
        super.setEnabledButton(false);
        appendButton();
    }

    private void appendNewIPRow() {
        TextField nameTextField = new TextField();
        nameTextField.setId("new-name-input");
        nameTextField.setPromptText("Name");
        nameTextField.setTooltip(new Tooltip("Name of the host."));
        TextField ipTextField = new TextField();
        ipTextField.setId("new-ip-input");
        ipTextField.setTooltip(new Tooltip("IP Address."));
        ipTextField.setPromptText("IP");
        ipTextField.setTextFormatter(new TextFormatter<>(new IPAddressValidator()));
        var doneButton = new Button("Confirm");
        doneButton.setOnAction(new ConfirmIPHandler(doneButton, nameTextField, ipTextField));
        gridPane.addRow(rowToBeAdded, nameTextField, ipTextField, doneButton);
    }

    private void appendButton() {
        gridPane.add(button, 1, gridPane.getRowCount());
    }

    private void removeOldRow() {
        gridPane.getChildren().remove(gridPane.getChildren().size() - 1);
    }

}
