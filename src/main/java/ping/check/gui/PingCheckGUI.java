/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui;

import ping.check.app.PropertiesManager;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public class PingCheckGUI {

    public static void main(String[] args) {
        configLog();
        LogMngr.info("Started ping checker, initiating GUI.");
        PropertiesManager.INSTANCE.loadProperties();
        GUI.startGUI(args);
    }

    private static void configLog() {
        System.setProperty("log4j.configurationFile", "log4j2.xml");
    }
}
