/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import ping.check.model.PingAddress;
import ping.check.model.PingAggregationResponse;
import ping.check.model.PingGUICounters;
import ping.check.model.PingResponse;

/**
 *
 * @author kkthe
 */
public class UpdatePingRunnable implements Runnable {

    public final PingAggregationResponse pingAggregation;
    public final BorderPane borderPane;

    public UpdatePingRunnable(PingAggregationResponse pingAggregation, BorderPane borderPane) {
        this.pingAggregation = pingAggregation;
        this.borderPane = borderPane;
    }

    @Override
    public void run() {
        VBox center = (VBox) borderPane.getCenter();
        ObservableList<Node> children = center.getChildren();
        Optional<Node> outputVBox = children.stream().filter(x -> "output".equals(x.getId())).findFirst();
        if (outputVBox.isPresent() && outputVBox.get() instanceof VBox) {
            VBox output = (VBox) outputVBox.get();
            output.getChildren().clear();
            output.setPadding(new Insets(5));
            Map<PingAddress, List<PingResponse>> pingMap = pingAggregation.getPingMap();
            var gridPane = new GridPane();
            gridPane.setAlignment(Pos.CENTER);
            gridPane.setPadding(new Insets(10, 10, 10, 10));
            gridPane.setVgap(10);
            gridPane.setHgap(5);
            gridPane.setPrefSize(250, 250);
            gridPane.add(new Text("Name"), 0, 0);
            var avgTextLabel = new Text("AVG");
            GridPane.setValignment(avgTextLabel, VPos.CENTER);
            GridPane.setHalignment(avgTextLabel, HPos.CENTER);
            gridPane.add(avgTextLabel, 1, 0);
            var pingCountTextLabel = new Text("Pings");
            GridPane.setValignment(pingCountTextLabel, VPos.CENTER);
            GridPane.setHalignment(pingCountTextLabel, HPos.CENTER);
            gridPane.add(pingCountTextLabel, 2, 0);
            var maxTextLabel = new Text("MAX");
            GridPane.setValignment(maxTextLabel, VPos.CENTER);
            GridPane.setHalignment(maxTextLabel, HPos.CENTER);
            gridPane.add(maxTextLabel, 3, 0);
            var minTextLabel = new Text("MIN");
            GridPane.setValignment(minTextLabel, VPos.CENTER);
            GridPane.setHalignment(minTextLabel, HPos.CENTER);
            gridPane.add(minTextLabel, 4, 0);
            var reachableTextLabel = new Text("Reachable");
            GridPane.setValignment(reachableTextLabel, VPos.CENTER);
            GridPane.setHalignment(reachableTextLabel, HPos.CENTER);
            gridPane.add(reachableTextLabel, 5, 0);
            var rowCounter = 1;
            for (Iterator<Map.Entry<PingAddress, List<PingResponse>>> it = pingMap.entrySet().iterator(); it.hasNext();) {
                Map.Entry<PingAddress, List<PingResponse>> pair = it.next();
                if (!pair.getKey().isEnabled()) {
                    it.remove();
                    continue;
                }
                var columnCounter = 0;
                PingAddress pingAddress = pair.getKey();
                List<PingResponse> pingResponses = pair.getValue();
                PingGUICounters counters = new PingGUICounters(pingResponses);
                gridPane.add(new Text(pingAddress.getName()), columnCounter++, rowCounter);
                gridPane.add(counters.getAverageLabel(), columnCounter++, rowCounter);
                gridPane.add(counters.getPingCountLabel(), columnCounter++, rowCounter);
                gridPane.add(counters.getMaxStr(), columnCounter++, rowCounter);
                gridPane.add(counters.getMinStr(), columnCounter++, rowCounter);
                gridPane.add(counters.getReachablePercentage(), columnCounter++, rowCounter);
                rowCounter++;
            }
            output.getChildren().add(gridPane);
        }
    }

}
