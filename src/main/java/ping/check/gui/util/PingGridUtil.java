/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.util;

import java.util.LinkedList;
import java.util.List;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import ping.check.app.Constants;
import ping.check.gui.button.handlers.AddNewIPHandler;
import ping.check.gui.button.handlers.CheckboxHandler;
import ping.check.model.PingAddress;

/**
 *
 * @author kkthe
 */
public enum PingGridUtil {
    INSTANCE;

    private final List<Control> IP_GRID_BUTTONS;
    private final GridPane gridPane;

    private PingGridUtil() {
        this.gridPane = new GridPane();
        this.IP_GRID_BUTTONS = new LinkedList<>();
    }

    public GridPane initGridPane() {
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(10, 30, 10, 10));
        gridPane.setId("pingAddresses");
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        addColumnLabels();
        appendIPsOnGrid();
        addNewIPButton();
        return gridPane;
    }

    public void redraw() {
        IP_GRID_BUTTONS.clear();
        gridPane.getChildren().clear();
        initGridPane();
    }

    public boolean isAnyIpEnabled() {
        return IP_GRID_BUTTONS.stream()
                .filter(x -> "address".equals(x.getId()))
                .filter((control) -> (control instanceof CheckBox))
                .anyMatch((checkbox) -> (((CheckBox) checkbox).isSelected()));
    }

    public void setButtonStatus(boolean enabled) {
        IP_GRID_BUTTONS.stream()
                .forEach(x -> x.setDisable(!enabled));
    }

    private void addColumnLabels() {
        final Font columnLabelsFont = new Font(16);
        var nameColumnLabel = new Label("Name");
        nameColumnLabel.setFont(columnLabelsFont);
        gridPane.add(nameColumnLabel, 0, 0);
        GridPane.setHalignment(nameColumnLabel, HPos.LEFT);
        GridPane.setValignment(nameColumnLabel, VPos.CENTER);
        var ipColumnLabel = new Label("IP");
        ipColumnLabel.setFont(columnLabelsFont);
        gridPane.add(ipColumnLabel, 1, 0);
        var enabledColumnLabel = new Label("Enabled");
        enabledColumnLabel.setFont(columnLabelsFont);
        gridPane.add(enabledColumnLabel, 2, 0);
    }

    private void appendIPsOnGrid() {
        int rowCounter = 1;
        for (PingAddress ping : Constants.PING_IPS) {
            var nameLabel = new Label(ping.getName());
            var ipLabel = new Label(ping.getHost());
            var enabledCheckbox = new CheckBox();
            enabledCheckbox.setId("address");
            enabledCheckbox.setSelected(ping.isEnabled());
            enabledCheckbox.setOnAction(new CheckboxHandler(enabledCheckbox, ping));
            IP_GRID_BUTTONS.add(enabledCheckbox);
            gridPane.add(nameLabel, 0, rowCounter);
            gridPane.add(ipLabel, 1, rowCounter);
            gridPane.add(enabledCheckbox, 2, rowCounter);
            GridPane.setHalignment(enabledCheckbox, HPos.CENTER);
            GridPane.setValignment(enabledCheckbox, VPos.CENTER);
            rowCounter++;
        }
    }

    private void addNewIPButton() {
        var button = new Button("Add new");
        button.setOnAction(new AddNewIPHandler(button, gridPane));
        IP_GRID_BUTTONS.add(button);
        gridPane.add(button, 1, gridPane.getRowCount());
    }

}
