/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.gui.util;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import ping.check.gui.GUI;

/**
 *
 * @author kkthe
 */
public enum BorderPaneUtil {
    INSTANCE;

    public void initBorderPane() {
        BorderPane borderPane = getBorderPane();
        borderPane.setId("mainPane");
        setMainBorderTopComponent();
        setMainBorderRightComponent();
        setMainBorderLeftComponent();
    }

    private void setMainBorderTopComponent() {
        Label topLabel = new Label("Ping Check");
        topLabel.setMaxWidth(Double.MAX_VALUE);
        topLabel.setFont(new Font(20));
        topLabel.setCenterShape(true);
        topLabel.setAlignment(Pos.CENTER);
        getBorderPane().setTop(topLabel);
    }

    private void setMainBorderRightComponent() {
        VBox rightVBox = new VBox();
        rightVBox.setId("rightVBox");
        rightVBox.setPadding(new Insets(0, 30, 0, 0));
        getBorderPane().setRight(rightVBox);
    }

    private void setMainBorderLeftComponent() {
        VBox leftVBox = new VBox();
        leftVBox.setId("leftVBox");
        leftVBox.setPadding(new Insets(0, 0, 0, 30));
        getBorderPane().setLeft(leftVBox);
    }

    private BorderPane getBorderPane() {
        return GUI.getBorderPane();
    }

}
