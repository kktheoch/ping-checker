/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.validator;

import ping.check.app.exception.InputValidationException;

/**
 *
 * @author kkthe
 */
public class InputValidation {

    public static int validateAndGetNumericInput(String text, int lowerLimit, int upperLimit) throws InputValidationException {
        if (text == null || text.isBlank()) {
            throw new InputValidationException("Input can't be null/empty");
        }
        try {
            int parsed = Integer.parseInt(text);
            if (parsed < lowerLimit || parsed > upperLimit) {
                throw new InputValidationException("Input " + text + " out of range.");
            }
            return parsed;
        } catch (NumberFormatException ex) {
            throw new InputValidationException("Input " + text + " is not valid numeric.");
        }
    }

    public static int validateAndGetInput(Integer input, int lowerLimit, int upperLimit) throws InputValidationException {
        if (input == null) {
            throw new InputValidationException("Input can't be null/empty");
        }
        if (input < lowerLimit || input > upperLimit) {
            throw new InputValidationException("Input " + String.valueOf(input) + " out of range.");
        }
        return input;
    }

}
