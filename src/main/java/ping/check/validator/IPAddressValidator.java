/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.validator;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import javafx.scene.control.TextFormatter.Change;
import ping.check.app.Constants;
import ping.check.app.exception.InputValidationException;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public class IPAddressValidator implements UnaryOperator<Change> {

    private static final Pattern IP_PATTERN = Pattern.compile("\\A(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\z");

    @Override
    public Change apply(Change change) {
        String regex = makePartialIPRegex();
        if (change.getControlNewText().matches(regex)) {
            return change;
        } else {
            return null;
        }
    }

    private String makePartialIPRegex() {
        String partialBlock = "(([01]?[0-9]{0,2})|(2[0-4][0-9])|(25[0-5]))";
        String subsequentPartialBlock = "(\\." + partialBlock + ")";
        String ipAddress = partialBlock + "?" + subsequentPartialBlock + "{0,3}";
        return "^" + ipAddress;
    }

    public static String validateIP(String ip, boolean checkDuplicate) throws InputValidationException {
        try {
            String validatedIP = doValidation(ip, checkDuplicate);
            LogMngr.info("IP " + ip + " valid. Result: " + validatedIP);
            return validatedIP;
        } catch (InputValidationException ex) {
            LogMngr.error("IP validation failed", ex);
            throw ex;
        }
    }

    private static String doValidation(String ip, boolean checkDuplicate) throws InputValidationException {
        if (ip == null) {
            throw new InputValidationException("IP can't be null.");
        }
        final String trimmedIP = ip.trim().toUpperCase();
        if (!IP_PATTERN.matcher(trimmedIP).matches()) {
            throw new InputValidationException("IP doesn't match regex.");
        }
        if (checkDuplicate) {
            boolean ipDuplicate = Constants.PING_IPS.stream()
                    .anyMatch(x -> trimmedIP.equals(x.getHost().toUpperCase()));
            if (ipDuplicate) {
                throw new InputValidationException("IP already exists.");
            }
        }
        return trimmedIP;
    }

}
