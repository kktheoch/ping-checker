/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.validator;

import ping.check.app.Constants;
import ping.check.app.exception.InputValidationException;
import ping.check.log.LogMngr;

/**
 *
 * @author kkthe
 */
public class NameValidator {

    public static String validate(String name) throws InputValidationException {
        try {
            String validated = doValidation(name);
            LogMngr.info("Validating name: " + name + " Result: " + validated);
            return validated;
        } catch (InputValidationException ex) {
            LogMngr.error("Validating name " + name + " failed", ex);
            throw ex;
        }
    }

    private static String doValidation(String name) throws InputValidationException {
        if (name == null || name.isBlank()) {
            throw new InputValidationException("Name can't be null");
        }
        final String trimmed = name.trim();
        boolean nameMatch = Constants.PING_IPS.stream()
                .anyMatch(x -> x.getName().toUpperCase().equals(trimmed.toUpperCase()));
        if (nameMatch) {
            throw new InputValidationException("Name already exists.");
        }
        return trimmed;
    }

}
