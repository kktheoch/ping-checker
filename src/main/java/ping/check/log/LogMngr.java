/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ping.check.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author kkthe
 */
public final class LogMngr {

    private static final Logger ERROR_LOGGER = LogManager.getLogger("error");
    private static final Logger INFO_LOGGER = LogManager.getLogger("info");

    public final static Logger getErrorLogger() {
        return ERROR_LOGGER;
    }

    public final static Logger getInfoLogger() {
        return INFO_LOGGER;
    }

    public final static void info(String msg) {
        info(msg, null);
    }

    public final static void info(String msg, Throwable ex) {
        INFO_LOGGER.info(msg, ex);
    }

    public final static void error(String msg) {
        error(msg, null);
    }

    public final static void error(String msg, Throwable ex) {
        ERROR_LOGGER.error(msg, ex);
    }

}
